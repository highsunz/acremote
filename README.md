# AC Remote

This repository contains an air conditioner's remote that is to be hosted on a raspberry pi 4.  The
user will operate through the [web interface](./src/main.js), and the web interface will post
control data to the [backend](./src/main.rs), the backend will then issue commands to actually send
infrared control signal (an ir sender hardware is needed).

## Usage

Building of this project is supported via [Nix][nix] [flakes][nix-flakes]:

```bash
$ # Enable the commands via /etc/nix/nix.conf or via command-line args like below:
$ alias nix='nix --extra-experimental-features nix-command --extra-experimental-features flakes'
$ nix build  # build the whole project
$ # Or ...
$ # nix build .#backend  # build the backend
$ # nix build .#frontend  # build the frontend (Vue.js)
$ # nix build .#frontend-elm  # build the fronted (Elm)
```

A binary cache is hosted at <https://hydra.blurgy.xyz>.

[nix]: <https://nixos.org/download.html#download-nix>
[nix-flakes]: <https://nixos.wiki/wiki/Flakes>
