{ runCommand
, version
}: runCommand "signals" {
  inherit version;
  src = ./.;
} ''
  mkdir $out/share/acremote/signals -p
  cp -r $src/{haier,midea,tcl} $out/share/acremote/signals
''
