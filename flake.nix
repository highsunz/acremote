{
  description = "Controling air-conditioner with a raspberry pi";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils, ... }: flake-utils.lib.eachSystem [ "aarch64-linux" "x86_64-linux" ] (system: let
    pkgs = import nixpkgs {
      inherit system;
      overlays = [ self.overlays.default ];
    };
  in {
    packages = {
      default = pkgs.acremote;
      backend = pkgs.acremote-backend;
      frontend = pkgs.acremote-frontend;
      frontend-elm = pkgs.acremote-frontend-elm;
      signals = pkgs.acremote-signals;
    };
    devShells = {
      frontend-elm = pkgs.mkShell {
        buildInputs = (with pkgs; [
          elm2nix
        ]) ++ (with pkgs.elmPackages; [
          elm
          elm-format
          elm-live  # elm-live -h 0 src/Main.elm --start-page=index.html -- --output=index.js
        ]);
        shellHook = ''
          [[ "$-" == *i* ]] && exec "$SHELL"
        '';
      };
    };
  }) // {
    hydraJobs = self.packages;
    nixosModules.default = import ./outputs/nixos-module.nix;
    overlays.default = let
      version = "0.3.0";
    in final: prev: rec {
      acremote = final.symlinkJoin {
        name = "acremote";
        paths = [
          acremote-backend
          #acremote-frontend
          acremote-frontend-elm
          acremote-signals
        ];
      };
      acremote-frontend = final.callPackage ./frontend { inherit version; };
      acremote-frontend-elm = final.callPackage ./frontend-elm { inherit version; };
      acremote-backend = final.callPackage ./backend { inherit version; };
      acremote-signals = final.callPackage ./signals { inherit version; };
    };
  };
}
