use color_eyre::Report;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use structopt::StructOpt;
use warp::Filter;

#[derive(StructOpt, Debug, Clone)]
#[structopt(
    global_settings(&[structopt::clap::AppSettings::ColoredHelp])
)]
pub struct Opt {
    #[structopt(help = "Which port to listen on", short, long)]
    pub listen_port: u16,
    #[structopt(help = "Which directory to serve as root", short, long)]
    pub server_root: PathBuf,
    #[structopt(help = "Which directory contains signal data", short, long)]
    pub data_root: PathBuf,
}

#[derive(Deserialize, Debug)]
struct ACState {
    on: bool,
    state: Option<State>,
}

#[derive(Deserialize, Debug)]
struct State {
    temperature: u8,
    mode: String,
    #[serde(rename = "fanSpeed")]
    fan_speed: String,
}

#[derive(Serialize, Debug)]
struct Resp {
    status: bool,
}

#[tokio::main]
async fn main() -> Result<(), Report> {
    setup()?;

    let opt = Opt::from_args();

    // Serve static assets from "/".
    let server_root = opt.server_root.clone();
    let static_from_root = warp::fs::dir(server_root).with(warp::log("static(from root)"));

    // Serve static assets with brands prefix (like "/haier/").
    let server_root = opt.server_root.clone();
    let static_with_brand = warp::path::param()
        .and(warp::fs::dir(server_root))
        .map(|_brand: String, file| file)
        .with(warp::log("static(with brand)"));

    // Handle posted data.
    let signal_sending = warp::path::param()
        .and(warp::path::end())
        .and(warp::post())
        .and(warp::body::json())
        .and_then(move |brand: String, d: ACState| {
            let brand_root = opt.data_root.join(&brand);
            async move {
                log::info!("Got posted data: {:#?}", d);
                let status = match d.on {
                    true => {
                        let state = d.state.unwrap();
                        std::process::Command::new("sudo")
                            .arg("ir-ctl")
                            .arg("--send")
                            .arg(
                                brand_root
                                    .join(state.mode)
                                    .join(state.fan_speed)
                                    .join(state.temperature.to_string()),
                            )
                            .spawn()
                    }
                    false => std::process::Command::new("sudo")
                        .arg("ir-ctl")
                        .arg("--send")
                        .arg(brand_root.join("off"))
                        .spawn(),
                }
                .is_ok();
                Ok::<_, warp::Rejection>(warp::reply::json(&Resp { status }))
            }
        })
        .with(warp::log("controlling"));

    let route = signal_sending.or(static_from_root).or(static_with_brand);

    warp::serve(route)
        .run(([0, 0, 0, 0], opt.listen_port))
        .await;

    Ok(())
}

fn setup() -> Result<(), Report> {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info");
    }

    color_eyre::install()?;
    pretty_env_logger::init();

    Ok(())
}

// Author: Blurgy <gy@blurgy.xyz>
// Date:   Aug 19 2021, 22:56 [CST]
