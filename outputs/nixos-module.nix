{ config, lib, pkgs, ... }: with lib; let
  cfg = config.services.acremote;
in {
  options = {
    services.acremote = {
      enable = mkEnableOption "Whether to enable a server to convert web requests to ir signals";
      package.frontend = mkOption { type = types.package; default = pkgs.acremote-frontend-elm; };
      package.backend = mkOption { type = types.package; default = pkgs.acremote-backend; };
      package.signals = mkOption { type = types.package; default = pkgs.acremote-signals; };
      port = mkOption {
        type = types.int;
        default = 12682;
      };
    };
  };

  config = mkIf cfg.enable {
    users.groups.infrared = { };
    users.extraUsers.acremote = {
      isSystemUser = true;
      group = "infrared";
    };
    security.sudo.extraRules = let
      applyNoPasswd = cmd: { command = cmd; options = [ "NOPASSWD" ]; };
      infraredCtlCmds = map
        applyNoPasswd
        (map (cmd: "${pkgs.v4l-utils}/bin/${cmd}") [ "ir-ctl" ]);
    in [
      { groups = [ "infrared" ]; commands = infraredCtlCmds; }
    ];

    environment.systemPackages = [
      pkgs.v4l-utils
    ];

    systemd.services.acremote = {
      wantedBy = [ "multi-user.target" ];
      script = ''
        # Add /run/wrappers/bin for `sudo` (''${pkgs.sudo}/bin won't work!)
        export PATH="''${PATH:+$PATH:}${pkgs.v4l-utils}/bin:/run/wrappers/bin"
        ${cfg.package.backend}/bin/acremote-backend \
          --data-root="${cfg.package.signals}/share/acremote/signals" \
          --listen-port="${builtins.toString cfg.port}" \
          --server-root="${cfg.package.frontend}/share/webapps/acremote"
      '';
      serviceConfig = {
        User = "acremote";
        Group = "infrared";
        Restart = "on-failure";
        RestartSec = 5;
      };
    };
  };
}
