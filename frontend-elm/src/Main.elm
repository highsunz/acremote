module Main exposing (main)

import Browser exposing (Document)
import Element exposing (centerX, column, el, htmlAttribute, image, padding, px, rgb255, row, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (onMouseEnter, onMouseLeave)
import Element.Font as Font
import Element.Input exposing (button)
import Html.Attributes as HA
import Http
import Json.Decode
import Json.Encode


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        }


type alias Model =
    { isOn : Bool
    , temperature : Int
    , mode : Mode
    , fanSpeed : FanSpeed

    -- Mouse hovering states
    , hoveringIcon : Bool
    , hoveringSetButton : Bool
    , hoveringModeButton : Bool
    , hoveringTemperatureButton : Bool
    , hoveringFanSpeedButton : Bool
    }


type Mode
    = Cool
    | Heat


type FanSpeed
    = Low
    | Medium
    | High
    | Auto


type Msg
    = Switch Bool
    | LoopNextTemperature
    | LoopNextFanSpeed
    | LoopNextMode
    | Execute
    | GotServerStatus (Result Http.Error String)
    | MouseOnIcon
    | MouseOnSetButton
    | MouseOnModeButton
    | MouseOnTemperatureButton
    | MouseOnFanSpeedButton
    | MouseOffIcon
    | MouseOffSetButton
    | MouseOffModeButton
    | MouseOffTemperatureButton
    | MouseOffFanSpeedButton


nextTemperature : Int -> Int
nextTemperature current =
    if current == 17 then
        20

    else if current == 20 then
        24

    else if current == 24 then
        26

    else if current == 26 then
        27

    else if current == 27 then
        28

    else if current == 28 then
        17

    else
        26


nextFanSpeed : FanSpeed -> FanSpeed
nextFanSpeed fanSpeed =
    case fanSpeed of
        Low ->
            Medium

        Medium ->
            High

        High ->
            Auto

        Auto ->
            Low


nextMode : Mode -> Mode
nextMode mode =
    case mode of
        Cool ->
            Heat

        Heat ->
            Cool


serverStatusDecoder : Json.Decode.Decoder String
serverStatusDecoder =
    Json.Decode.field "status" Json.Decode.string


modelEncoder : Model -> Json.Encode.Value
modelEncoder model =
    Json.Encode.object <|
        [ ( "on"
          , Json.Encode.bool model.isOn
          )
        , ( "state"
          , if model.isOn then
                Json.Encode.object
                    [ ( "mode"
                      , Json.Encode.string (modeToString model.mode)
                      )
                    , ( "fanSpeed"
                      , Json.Encode.string
                            (fanSpeedToString model.fanSpeed)
                      )
                    , ( "temperature", Json.Encode.int model.temperature )
                    ]

            else
                Json.Encode.null
          )
        ]


fanSpeedToString : FanSpeed -> String
fanSpeedToString fanSpeed =
    case fanSpeed of
        Low ->
            "low"

        Medium ->
            "medium"

        High ->
            "high"

        Auto ->
            "auto"


modeToString : Mode -> String
modeToString mode =
    case mode of
        Cool ->
            "cool"

        Heat ->
            "heat"


execute : Model -> Cmd Msg
execute model =
    Http.post
        { url = ""
        , body = Http.jsonBody <| modelEncoder model
        , expect = Http.expectJson GotServerStatus serverStatusDecoder
        }


init : flags -> ( Model, Cmd Msg )
init _ =
    ( { isOn = True
      , temperature = 26
      , mode = Heat
      , fanSpeed = Auto
      , hoveringIcon = False
      , hoveringSetButton = False
      , hoveringModeButton = False
      , hoveringTemperatureButton = False
      , hoveringFanSpeedButton = False
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    ( case msg of
        Switch onOffStatus ->
            { model | isOn = onOffStatus }

        LoopNextTemperature ->
            { model | temperature = nextTemperature model.temperature }

        LoopNextFanSpeed ->
            { model | fanSpeed = nextFanSpeed model.fanSpeed }

        LoopNextMode ->
            { model | mode = nextMode model.mode }

        Execute ->
            { model | isOn = True }

        GotServerStatus _ ->
            model

        MouseOnIcon ->
            { model | hoveringIcon = True }

        MouseOnSetButton ->
            { model | hoveringSetButton = True }

        MouseOnModeButton ->
            { model | hoveringModeButton = True }

        MouseOnTemperatureButton ->
            { model | hoveringTemperatureButton = True }

        MouseOnFanSpeedButton ->
            { model | hoveringFanSpeedButton = True }

        MouseOffIcon ->
            { model | hoveringIcon = False }

        MouseOffSetButton ->
            { model | hoveringSetButton = False }

        MouseOffModeButton ->
            { model | hoveringModeButton = False }

        MouseOffTemperatureButton ->
            { model | hoveringTemperatureButton = False }

        MouseOffFanSpeedButton ->
            { model | hoveringFanSpeedButton = False }
    , case msg of
        Execute ->
            if model.isOn then
                execute model

            else
                execute { model | isOn = True }

        Switch onOffStatus ->
            execute { model | isOn = onOffStatus }

        _ ->
            Cmd.none
    )


view : Model -> Document Msg
view model =
    { title = "AC Remote"
    , body =
        [ Element.layout
            [ Background.color (rgb255 0x4A 0x78 0xA1) ]
            (column
                [ spacing 20
                , centerX
                ]
                [ row rowAttrs
                    [ button
                        (buttonAttrs model.hoveringIcon MouseOnIcon MouseOffIcon)
                        { label =
                            image
                                [ width (px 192)
                                , htmlAttribute
                                    (HA.style "filter"
                                        ("grayscale("
                                            ++ (if model.isOn then
                                                    "0"

                                                else
                                                    "100"
                                               )
                                            ++ "%)"
                                        )
                                    )
                                ]
                                { src = "/assets/ac.svg", description = "An icon of an air conditioner" }
                        , onPress =
                            if model.isOn then
                                Just (Switch False)

                            else
                                Nothing
                        }
                    ]
                , row rowAttrs
                    [ button
                        ([ Background.color (rgb255 0x22 0x58 0x7B)
                         , Font.color (rgb255 0xFF 0xFF 0xFF)
                         , width <| px 292
                         ]
                            ++ buttonAttrs model.hoveringSetButton MouseOnSetButton MouseOffSetButton
                        )
                        { label =
                            el
                                elAttrs
                                (text ("Set: " ++ modeToString model.mode ++ "-" ++ fanSpeedToString model.fanSpeed ++ "-" ++ String.fromInt model.temperature))
                        , onPress = Just Execute
                        }
                    ]
                , row rowAttrs
                    [ button
                        (buttonAttrs model.hoveringModeButton MouseOnModeButton MouseOffModeButton
                            ++ [ Background.color (rgb255 0xEF 0x9E 0x3D)
                               , width <| px 200
                               , padding 16
                               ]
                        )
                        { label =
                            el
                                elAttrs
                                (text ("mode: " ++ modeToString model.mode))
                        , onPress = Just LoopNextMode
                        }
                    ]
                , row rowAttrs
                    [ button
                        (buttonAttrs model.hoveringTemperatureButton MouseOnTemperatureButton MouseOffTemperatureButton
                            ++ [ Background.color (rgb255 0x69 0xFF 0xC3)
                               , width <| px 224
                               , padding 16
                               ]
                        )
                        { label =
                            el
                                elAttrs
                                (text ("temperature: " ++ String.fromInt model.temperature))
                        , onPress = Just LoopNextTemperature
                        }
                    ]
                , row rowAttrs
                    [ button
                        (buttonAttrs model.hoveringFanSpeedButton MouseOnFanSpeedButton MouseOffFanSpeedButton
                            ++ [ Background.color (rgb255 0x8A 0xD3 0x33)
                               , width <| px 224
                               , padding 16
                               ]
                        )
                        { label =
                            el
                                elAttrs
                                (text ("fan speed: " ++ fanSpeedToString model.fanSpeed))
                        , onPress = Just LoopNextFanSpeed
                        }
                    ]
                ]
            )
        ]
    }


rowAttrs : List (Element.Attribute Msg)
rowAttrs =
    [ centerX ]


buttonAttrs : Bool -> Msg -> Msg -> List (Element.Attribute Msg)
buttonAttrs hoveringStatus mouseOnMsg mouseOffMsg =
    [ padding 32
    , centerX
    , Border.rounded 999
    , htmlAttribute <|
        HA.style "filter"
            ("brightness("
                ++ (if hoveringStatus then
                        "70"

                    else
                        "100"
                   )
                ++ "%)"
            )
    , htmlAttribute <| HA.style "box-shadow" "none"
    , onMouseEnter mouseOnMsg
    , onMouseLeave mouseOffMsg
    ]


elAttrs : List (Element.Attribute Msg)
elAttrs =
    [ centerX
    , Font.family [ Font.monospace ]
    ]
