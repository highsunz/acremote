{ stdenv
, version
, elmPackages
}: stdenv.mkDerivation {
  pname = "frontend-elm";
  inherit version;
  src = ./.;
  buildInputs = [ elmPackages.elm ];
  buildPhase = elmPackages.fetchElmDeps {
    # run elm2nix in frontend-elm
    elmPackages = import ./elm-srcs.nix;
    elmVersion = "0.19.1";
    registryDat = ./registry.dat;
  };
  installPhase = ''
    install -Dm644 -t $out/share/webapps/acremote $src/index.html
    install -Dm644 -t $out/share/webapps/acremote/assets $src/assets/ac.svg
    elm make $src/src/Main.elm --output=$out/share/webapps/acremote/index.js
  '';
}
