{ mkYarnPackage
, version
}: mkYarnPackage {
  pname = "acremote-frontend";
  inherit version;
  src = ./.;
  distPhase = "true";
  yarnLock = ./yarn.lock;
  configurePhase = "ln -s $node_modules node_modules";
  buildPhase = ''
    export HOME=$(mktemp -d)
    yarn --offline build
  '';
  installPhase = ''
    mkdir -p $out/share/webapps
    mv dist $out/share/webapps/acremote
  '';
}
